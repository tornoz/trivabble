(function (that) {
    var divAlert, divAlertInput, divAlertConfirm, divAlertButton, alertButtonOK,
        divAlertCallback, divAlertCallbackYes, divAlertCallbackNo, alertInput;

    var _ = (window.libD && libD.l10n) ? libD.l10n() : function (s) { return s; };

    function promptOK() {
        divAlert.style.display = "none";
        divAlertCallback && divAlertCallback(alertInput.value);
    }

    function promptCancel() {
        divAlert.style.display = "none";
        divAlertCallback && divAlertCallback(null);
    }

    function confirmYes() {
        divAlert.style.display = "none";
        divAlertCallbackYes && divAlertCallbackYes();
    }

    function confirmNo() {
        divAlert.style.display = "none";
        divAlertCallbackNo && divAlertCallbackNo();
    }

    function alertOK() {
        divAlert.style.display = "none";
        divAlertCallback && divAlertCallback();
    }

    function prepare() {
        divAlert = document.createElement("div");
        divAlert.className = "alert";
        divAlert.style.display = "none";

        divAlertContent = document.createElement("div");
        divAlertContent.className = "alert-content";
        divAlertContent.style.whiteSpace = "pre-wrap";

        divAlertInput = document.createElement("div");
        divAlertInput.className = "alert-prompt";

        alertInput = document.createElement("input");
        alertInput.type = "text";
        alertInput.onkeydown = function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                promptOK();
            }
        };

        divAlertInput.appendChild(alertInput);

        divAlertInput.appendChild(document.createElement("div"));
        divAlertInput.lastChild.className = "alert-prompt-buttons";
        divAlertInput.lastChild.appendChild(document.createElement("button"));
        divAlertInput.lastChild.lastChild.textContent = _("OK");
        divAlertInput.lastChild.lastChild.onclick = promptOK;
        divAlertInput.lastChild.appendChild(document.createElement("button"));
        divAlertInput.lastChild.lastChild.textContent = _("Annuler");
        divAlertInput.lastChild.lastChild.onclick = promptCancel;

        divAlertConfirm = document.createElement("div");
        divAlertConfirm.className = _("alert-confirm");
        divAlertConfirm.appendChild(document.createElement("button"));
        divAlertConfirm.lastChild.textContent = _("Oui");
        divAlertConfirm.lastChild.onclick = confirmYes;
        divAlertConfirm.appendChild(document.createElement("button"));
        divAlertConfirm.lastChild.textContent = _("Non");
        divAlertConfirm.lastChild.onclick = confirmNo;

        divAlertButton = document.createElement("div");
        alertButtonOK = document.createElement("button");
        divAlertButton.appendChild(alertButtonOK);
        alertButtonOK.textContent = _("OK");
        alertButtonOK.onclick = alertOK;

        var divAlertOuter = document.createElement("div");
        divAlertOuter.className = "alert-outer";
        divAlertOuter.appendChild(divAlertContent);
        divAlertOuter.appendChild(divAlertInput);
        divAlertOuter.appendChild(divAlertConfirm);
        divAlertOuter.appendChild(divAlertButton);
        divAlert.appendChild(divAlertOuter);
        document.body.appendChild(divAlert);
    }

    that.myAlert = function (msg, callback) {
        if (!divAlert) {
            prepare();
        }

        divAlertContent.textContent = msg;
        divAlertInput.style.display = "none";
        divAlertConfirm.style.display = "none";
        divAlertButton.style.display = "";
        divAlertCallback = callback;
        divAlert.style.display = "";
        divAlertButton.getElementsByTagName("button")[0].focus();
    };

    that.myAlert.l10n = _;

    that.myPrompt = function (msg, callback, defaultText) {
        if (!divAlert) {
            prepare();
        }

        divAlertContent.textContent = msg;
        divAlertInput.style.display = "";
        divAlertInput.firstChild.value = defaultText || "";
        divAlertConfirm.style.display = "none";
        divAlertButton.style.display = "none";
        divAlertCallback = callback;
        divAlert.style.display = "";

        if (!/iPad|iPhone|iPod/g.test(navigator.userAgent)) {
            alertInput.focus();
            alertInput.setSelectionRange(0, alertInput.value.length);
            alertInput.select();
        }
    };

    that.myConfirm = function (msg, callbackYes, callbackNo) {
        if (!divAlert) {
            prepare();
        }

        divAlertContent.textContent = msg;
        divAlertInput.style.display = "none";
        divAlertConfirm.style.display = "";
        divAlertButton.style.display = "none";
        divAlertCallbackYes = callbackYes;
        divAlertCallbackNo = callbackNo;
        divAlert.style.display = "";
        divAlertConfirm.getElementsByTagName("button")[0].focus();
    };
}(this));
