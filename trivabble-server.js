/**
 * Copyright (C) 2016 Raphaël Jakse <raphael.jakse@gmail.com>
 *
 * @licstart
 * This file is part of Trivabble.
 *
 * Trivabble is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License (GNU AGPL)
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Trivabble is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Trivabble.  If not, see <http://www.gnu.org/licenses/>.
 * @licend
 *
 * @source: https://trivabble.1s.fr/
 * @source: https://gitlab.com/raphj/trivabble/
 */

const port = 3000;
const SAVE_TIMEOUT = 5000;

var http = require("http");
var fs   = require("fs");

var bags = {
  'fr':[
      " ", " ", // jokers
      "E","E","E","E","E","E","E","E","E","E","E","E","E","E","E",
      "A","A","A","A","A","A","A","A","A",
      "I","I","I","I","I","I","I","I",
      "N","N","N","N","N","N",
      "O","O","O","O","O","O",
      "R","R","R","R","R","R",
      "S","S","S","S","S","S",
      "T","T","T","T","T","T",
      "U","U","U","U","U","U",
      "L","L","L","L","L",
      "D","D","D",
      "M","M","M",
      "G","G",
      "B","B",
      "C","C",
      "P","P",
      "F","F",
      "H","H",
      "V","V",
      "J",
      "Q",
      "K",
      "W",
      "X",
      "Y",
      "Z"
  ],
  'br':[
      " ", " ", // jokers
      "E","E","E","E","E","E","E","E","E","E","E","E","E","E",
      "A","A","A","A","A","A","A","A","A",
      "N","N","N","N","N","N","N","N","N",
      "R","R","R","R","R","R","R",
      "O","O","O","O","O","O",
      "T","T","T","T","T",
      "U","U","U","U","U",
      "I","I","I","I",
      "L","L","L","L",
      "D","D","D","D",
      "V","V","V",
      "S","S","S",
      "G","G","G",
      "M","M",
      "ZH","ZH",
      "Z","Z",
      "K","K",
      "H","H",
      "B","B",
      "P",
      "F",
      "J",
      "CH",
      "C'H",
      "W",
      "Y",
  ]
};

var values = {
  'br':{
      " ": 0,
      "E": 1,
      "A": 1,
      "I": 1,
      "N": 1,
      "O": 1,
      "R": 1,
      "S": 3,
      "T": 1,
      "U": 1,
      "L": 1,
      "D": 2,
      "M": 4,
      "G": 3,
      "B": 4,
      "CH": 4,
      "C'H": 4,
      "P": 5,
      "F": 10,
      "H": 3,
      "V": 3,
      "J": 10,
      "K": 4,
      "W": 10,
      "Y": 10,
      "Z": 4,
      "ZH": 4
  },
  'fr': {
    " ": 0,
    "E": 1,
    "A": 1,
    "I": 1,
    "N": 1,
    "O": 1,
    "R": 1,
    "S": 1,
    "T": 1,
    "U": 1,
    "L": 1,
    "D": 2,
    "M": 2,
    "G": 2,
    "B": 3,
    "C": 3,
    "P": 3,
    "F": 4,
    "H": 4,
    "V": 4,
    "J": 8,
    "Q": 8,
    "K": 10,
    "W": 10,
    "X": 10,
    "Y": 10,
    "Z": 10
  }
}
var defaultLang = 'fr';


var games = {};
var dateNow = 0;

var saveTo = null;

function saveGames() {
    fs.writeFile('games.backup.json', JSON.stringify(games), function (err) {
        if (err) {
            console.error('ERROR: Cannot save games!');
        }
    });

    saveTo = null;
}

function Game(lang) {
    this.letterValues = values[lang];
    this.lang = lang;
    this.init();
    this.listeningPlayers = [];
    this.pendingEvents = [];
}

function writeResponse(response, data, terminate) {
    response[terminate ? "end" : "write"](data.length + data);
}

function newBoard() {
    var res = new Array(15 * 15);

    for (var i = 0; i < 15 * 15; i++) {
        res[i] = "";
    }

    return res;
}

Game.prototype.init = function () {
    this.board = newBoard();
    this.bag = bags[this.lang].slice();
    this.remainingLetters = this.bag.length;
    this.racks = {};
    this.scores = {};
}

Game.prototype.toJSON = function () {
    return {
        remainingLetters: this.remainingLetters,
        board:            this.board,
        bag:              this.bag,
        racks:            this.racks,
        scores:           this.scores,
        lang:             this.lang
    };
};

Game.fromJSON = function (obj) {
    var game = new Game();
    game.board            = obj.board  || newBoard();
    game.remainingLetters = obj.remainingLetters || game.bag.length;
    game.racks            = obj.racks  || {};
    game.scores           = obj.scores || {};
    game.lang             = obj.lang   || defaultLang;
    game.bag              = obj.bag    || bags[game.lang].slice();
    return game;
};

Game.prototype.getPlayerRack = function (player) {
    var playerID = "#" + player;
    return (this.racks[playerID] || (this.racks[playerID] = []));
};

Game.prototype.getPlayerScore = function (player) {
    var playerID = "#" + player;
    return (this.scores[playerID] || (this.scores[playerID] = 0));
};

Game.prototype.setPlayerScore = function (player, score) {
    var playerID = "#" + player;

    if (!this.racks.hasOwnProperty(playerID) || typeof score !== "number") {
        return;
    }

    this.scores[playerID] = score;

    this.pendingEvents.push(
        {
            players: [{
                player: player,
                score: score
            }],
            date: dateNow
        }
    );
};

Game.prototype.playerJoined = function (playerName) {
    if (playerName) {
        this.getPlayerRack(playerName); // create the player's rack
    }

    var players = [];

    for (var player in this.racks) {
        if (this.racks.hasOwnProperty(player)) {
            player = player.slice(1); // '#'
            players.push(
                {
                    player: player,
                    score: this.getPlayerScore(player),
                    rackCount: countTiles(this.getPlayerRack(player))
                }
            );
        }
    }

    this.pendingEvents.push(
        {
            players: players,
            date: dateNow
        }
    );
};

Game.prototype.addListeningPlayer = function (playerName, response) {
    var that = this;

    that.listeningPlayers.push(response);

    response.on("close", function () {
        var index = that.listeningPlayers.indexOf(response);
        if (index !== -1) {
            that.listeningPlayers[index] = that.listeningPlayers[that.listeningPlayers.length - 1];
            that.listeningPlayers.pop();
        }
    });

    this.playerJoined(playerName);
    this.commit();
};

Game.prototype.commit = function () {
    var pendingEvents = this.pendingEvents;
    this.pendingEvents = [];
    msg = JSON.stringify(pendingEvents);

    for (var i = 0; i < this.listeningPlayers.length; i++) {
        while (i < this.listeningPlayers.length && !this.listeningPlayers[i]) {
            this.listeningPlayers[i] = this.listeningPlayers[this.listeningPlayers.length - 1];
            that.listeningPlayers.pop();
        }

        if (this.listeningPlayers[i]) {
            writeResponse(this.listeningPlayers[i], msg);
        }
    }

    if (saveTo === null) {
        saveTo = setTimeout(saveGames, SAVE_TIMEOUT);
    }
};

Game.prototype.bagPopLetter = function (player) {
    if (this.remainingLetters) {
        var letter = "";
        var index
        while (letter === "") {
            index = Math.floor(Math.random() * this.bag.length);
            letter = this.bag[index];
        }

        this.bag[index] = "";
        this.remainingLetters--;
        this.pendingEvents.push(
            {
                player: player,
                action: "popBag",
                remainingLetters: this.remainingLetters,
                date: dateNow
            }
        );
        return letter;
    }

    return "";
};

Game.prototype.getCell = function (index) {
    return this.board[index];
}

Game.prototype.setCell = function (index, letter, player) {
    this.board[index] = letter;
    this.pendingEvents.push(
        {
            player:player,
            action:"setCell",
            indexTo:index,
            letter:letter,
            date: dateNow
        }
    );
};

Game.prototype.bagPushLetter = function (letter, player) {
    if (letter) {
        var index = 0;

        while (this.bag[index] !== "") {
            index++;
        }

        this.bag[index] = letter;

        this.remainingLetters++;

        this.pendingEvents.push(
            {
                player: player,
                action: "pushBag",
                remainingLetters: this.remainingLetters,
                date: dateNow
            }
        );
    }
};

Game.prototype.reset = function (player) {
    this.init();
    this.pendingEvents.push(
        {
            player: player,
            action: "reset",
            board: this.board,
            remainingLetters: this.remainingLetters,
            rack: [],
            date: dateNow
        }
    );

    this.playerJoined();
};

function newGameId() {
    var number;

    var k = 10000;
    var retries = 0;

    do {
        number = Math.floor(Math.random() * k).toString();

        if (retries > 10) {
            retries = 0;
            k *= 10;
        } else {
            retries++;
        }
    } while (games[number]);

    return number.toString();
}

destToAction = {
    "rack": "setRackCell",
    "board": "setCell",
    "bag": "pushBag"
}

function countTiles(rack) {
    var count = 0;

    for (var i = 0; i < rack.length; i++) {
        if (rack[i]) {
            count++;
        }
    }

    return count;
}

function handleCommand(cmd, gameNumber, playerName, response) {
    var game = games[gameNumber];

    if (!game && (cmd.cmd !== "joinGame")) {
        response.write('{"error":1,"reason":"Missing or bad game number"}');
        return;
    }

    var rack = null;

    switch (cmd.cmd) {
        case "joinGame":
            if (!gameNumber) {
                gameNumber = newGameId();
            }

            if (!game) {
                game = games[gameNumber] = new Game(cmd.lang);
            }

            game.playerJoined(playerName);

            response.write(
                JSON.stringify(
                    {
                        "error":0,
                        "gameNumber": gameNumber,
                        "playerName": playerName,
                        "rack": game.getPlayerRack(playerName),
                        "board": game.board,
                        "remainingLetters": game.remainingLetters,
                        "letterValues": game.lang,
                        "date": dateNow,
                        "sync": false
                    }
                )
            );
            break;

        case "hello":
            game.playerJoined(playerName);
            break;

        case "sync":
            response.write(
                JSON.stringify(
                    {
                        "error":0,
                        "gameNumber": gameNumber,
                        "playerName": playerName,
                        "rack": game.getPlayerRack(playerName),
                        "board": game.board,
                        "remainingLetters": game.remainingLetters,
                        "letterValues": values[game.lang],
                        "date": dateNow,
                        "sync": true
                    }
                )
            );
            break;

        case "score":
            game.setPlayerScore(cmd.player, cmd.score);
            response.write('{"error":0}');
            break;

        case "moveLetter":
            var letter = "";

            switch (cmd.from) {
                case "rack":
                    rack = game.getPlayerRack(playerName);
                    letter = rack[cmd.indexFrom];
                    rack[cmd.indexFrom] = "";
                    updateRack = true;
                    break;

                case "board":
                    if (cmd.indexFrom < 15 * 15) {
                        letter = game.getCell(cmd.indexFrom);
                        game.setCell(cmd.indexFrom, "", playerName);
                    }
                    break;

                case "bag":
                    letter = game.bagPopLetter(playerName);
                    break;

                default:
                    response.write('{"error":1, "reason":"Moving letter from an unknown place"}');
                    return;
            }

            switch (cmd.to) {
                case "rack":
                    if (cmd.indexTo > 7) {
                        response.write('{"error":1, "reason":"Moving letter to a index which is too high"}');
                        return;
                    }

                    rack = rack || game.getPlayerRack(playerName);
                    rack[cmd.indexTo] = letter;
                    break;

                case "board":
                    if (cmd.indexTo < 15 * 15) {
                        game.setCell(cmd.indexTo, letter, playerName);
                    }
                    break;

                case "bag":
                    game.bagPushLetter(letter, playerName);
                    break;

                default:
                    switch (cmd.from) {
                        case "rack":
                            rack[cmd.indexFrom] = letter;
                            break;

                        case "board":
                            game.setCell(cmd.indexFrom, letter, playerName);
                            break;

                        case "bag":
                            game.bagPushLetter(letter, playerName);
                            break;

                        default:
                            console.error("BUG: code should not have been reached.");
                    }

                    response.write('{"error":1, "reason":"Moving letter to an unknown place"}');
                    return;
            }

            response.write(
                JSON.stringify(
                    {
                        error:0,
                        letter: letter,
                        action: "moveLetter",
                        indexTo: typeof cmd.indexTo === "number" ? cmd.indexTo : -1,
                        from: cmd.from,
                        to: cmd.to,
                        indexFrom: typeof cmd.indexFrom === "number" ? cmd.indexFrom : -1,
                        remainingLetters: game.remainingLetters,
                        date: dateNow
                    }
                )
            );

            if (rack) {
                game.pendingEvents.push(
                    {
                        players: [{
                            player: playerName,
                            rackCount: countTiles(rack)
                        }],
                        date: dateNow
                    }
                );
            }
            break;

        case "setRack":
            cmd.rack.length = 8;

            var rack = game.getPlayerRack(playerName);

            var initialRackCount = countTiles(rack);
            var rackCount = countTiles(cmd.rack);

            console.log(rack, cmd.rack);

            if (initialRackCount !== rackCount) {
                response.write(
                    JSON.stringify({
                        error: 1,
                        rack: rack,
                        reason: "the new rack doesn't contain the same number of letters",
                        date: dateNow
                    })
                );
                break;
            }

            var oldRack = rack.slice();


            for (var i = 0; i < 8; i++) {
                cmd.rack[i] = (cmd.rack[i] || "")[0] || "";
                if (cmd.rack[i]) {
                    var indexLetter = oldRack.indexOf(cmd.rack[i]);

                    if (indexLetter === -1) {
                        response.write(
                            JSON.stringify({
                                error: 1,
                                rack: rack,
                                reason: "the new rack is not a permutation of the old rack",
                                date: dateNow
                            })
                        );
                        break;
                    }
                    oldRack[indexLetter] = false;
                }
            }

            for (var i = 0; i < 8; i++) {
                rack[i] = cmd.rack[i];
            }

            response.write(
                JSON.stringify({
                    error: 0,
                    rack: rack,
                    date: dateNow
                })
            );

            break;
        case "resetGame":
            game.reset();
            response.write('{"error":0}');
            break;

        case "msg":
            game.pendingEvents.push(
                {
                    msg: {
                        sender: playerName,
                        content: cmd.msg
                    },
                    date: dateNow
                }
            );
            response.write('{"error":0}');
            break;

        default:
            response.write('{"error":1,"reason":"Unknown command"}');
            break;
    }
}

function handleCommands(cmds, response) {
    if (!cmds.cmds || !cmds.cmds.length) {
        var game = games[cmds.gameNumber];

        if (!game) {
            writeResponse(response, '{"error":1,"reason":"Missing or bad game number"}', true);
            return;
        }

        writeResponse(response,
            JSON.stringify(
                {
                    "playerName":       cmds.playerName,
                    "gameNumber":       cmds.gameNumber,
                    "letterValues":     game.letterValues,
                    "rack":             game.getPlayerRack(cmds.playerName),
                    "board":            game.board,
                    "remainingLetters": game.remainingLetters,
                    "date": dateNow
                }
            )
        );

        game.addListeningPlayer(cmds.playerName, response);
        return;
    }

    response.write("[");

    for (var i = 0; i < cmds.cmds.length; i++) {
        if (i) {
            response.write(",");
        }
        handleCommand(cmds.cmds[i], cmds.gameNumber, cmds.playerName, response);
    }

    response.end("]");

    if (games[cmds.gameNumber]) {
        console.log("pendingEvents COMMIT", games[cmds.gameNumber].pendingEvents);
        games[cmds.gameNumber].commit();
    }
}

function handleRequest(request, response) {
    var post = '';

    // thx http://stackoverflow.com/questions/4295782/how-do-you-extract-post-data-in-node-js
    request.on("data", function (data) {
        post += data;

        // Too much POST data, kill the connection!
        if (post.length > 1e6) {
            request.connection.destroy();
        }
    });

    request.on("end", function () {
        response.setHeader("Content-Type",   "text/plain");
        response.setHeader("Transfer-Encoding", "chunked");
        dateNow = Date.now();
        console.log("RECEIVED", post);
        handleCommands(post && JSON.parse(post), response);
    });
}

fs.readFile('games.backup.json', function (err, data) {
    try {
        if (err) {
            console.error("WARNING: Could not restore previous backup of the games");
        } else {
            var backup = JSON.parse(data);

            for (var gameNumber in backup) {
                games[gameNumber] = Game.fromJSON(backup[gameNumber]);
            }
        }
    } catch (e) {
        console.error("WARNING: Could not restore previous backup of the games: file is broken:");
        console.error("WARNING: ", e);
    }

    http.createServer(handleRequest).listen(port, function() {
        console.log("Server listening on: http://localhost:%s", port);
    });
});
