(function () {
    "use strict";

    var POLLING_DELAY = 5000;

    var _ = (window.libD && libD.l10n) ? libD.l10n() : function (s) { return s; };

    window.trivabble = {l10n: _};

    function format(s, v) {
        return s.replace("{0}", v);
    }

    var newGameBtn, board, rack, boardCells = [], scoreOf, bag;

    var playerLetters = [];

    var audioNotification, audioChat;
    var chatMessages, helpBag, helpClear;

    var tablePlayers = {};
    var participantPlaceholder, participants;

    var lastDate = 0;

    var blockMove = 0;
    var gameInProgress = false;

    function onclick(ele, fun) {
        ele.addEventListener("click", fun, false);
        ele.addEventListener("touch", fun, false);
    }

    function mouseDown(ele, fun, stop) {
        var meth = stop ? "removeEventListener" : "addEventListener";
        ele[meth]("mousedown", fun, false);
    }

    function mouseUp(ele, fun, stop) {
        var meth = stop ? "removeEventListener" : "addEventListener";
        ele[meth]("mouseup", fun, false);
    }

    function mouseMove(ele, fun, stop) {
        var meth = stop ? "removeEventListener" : "addEventListener";
        ele[meth]("mousemove", fun, false);
    }

    function setRack(rack) {
        for (var i = 0; i < 7; i++) {
            setTileParent(playerLetters[i], rack[i] || "");
        }
    }

    function getFreeRackSpaceIndex() {
        for (var i = 0; i < 7; i++) {
            if (!playerLetters[i].getElementsByClassName("tile")[0]) {
                return i;
            }
        }

        return -1;
    }

    var tileInitCoords      = null,
        tileInitMouseCoords = null,
        tileDest            = null,
        tileInitDest        = null,
        movingTile          = null,
        rackBCR             = null,
        boardBCR            = null,
        bagBCR              = null,
        moveCMD             = null;

    function getLetter(l) {
        var tile = l.getElementsByClassName("tile")[0];
        if (!tile) {
            return "";
        }

        return tile.getElementsByClassName("tile-letter")[0].textContent;
    }

    function isParentOf(p, elem) {
        while (elem) {
            if (elem === p) {
                return true;
            }
            elem = elem.parentNode;
        }

        return false;
    }

    function dragTileEnd(e) {
        movingTile.style.left   = "";
        movingTile.style.top    = "";
        movingTile.style.width  = "";
        movingTile.style.height = "";

        mouseUp(document,   dragTileEnd,  true);
        mouseMove(document, dragTileMove, true);

        if (tileDest === bag) {
            moveCMD.to = "bag";
            moveCMD.indexTo = -1;
            movingTile.parentNode.removeChild(movingTile);
        } else if (tileDest) {
            tileDest.appendChild(movingTile);

            if (isParentOf(board, tileDest)) {
                var tiles = [].slice.call(board.getElementsByClassName("tile-highlight"));
                var len = tiles.length;

                for (var i = 0; i < len; i++) {
                    tiles[i].classList.remove("tile-highlight");
                }
            }
        } else if (tileInitDest.getElementsByClassName("tile")[0]) {
            for (var i = 0; i < 7; i++) {
                if (!playerLetters[i].getElementsByClassName("tile")[0]) {
                    playerLetters[i].appendChild(movingTile);
                    break;
                }
            }
        } else {
            tileInitDest.appendChild(movingTile);
        }

        if (tileDest) {
            tileDest.classList.remove("tile-target");

            var moveRack = {
                cmd: "setRack",
                rack: [
                    getLetter(playerLetters[0]),
                    getLetter(playerLetters[1]),
                    getLetter(playerLetters[2]),
                    getLetter(playerLetters[3]),
                    getLetter(playerLetters[4]),
                    getLetter(playerLetters[5]),
                    getLetter(playerLetters[6])
                ]
            };

            if (moveCMD.to === moveCMD.from && (moveCMD.indexTo === moveCMD.indexFrom || moveCMD.from === "rack")) {
                sendCmds([moveRack]);
            } else {
                sendCmds([moveCMD, moveRack]);
            }
        }

        tileInitMouseCoords = null;
        tileInitCoords      = null;
        tileInitDest        = null;
        tileDest            = null;
        movingTile          = null;
        boardBCR            = null;
        bagBCR              = null;
        rackBCR             = null;
        moveCMD             = null;
    }

    var DBG = 0;
    function dragTileMove(e) {
        var newLeft = (tileInitCoords.left + (e.clientX - tileInitMouseCoords.clientX));
        var newTop  = (tileInitCoords.top  + (e.clientY - tileInitMouseCoords.clientY));

        movingTile.style.left = newLeft  + "px";
        movingTile.style.top  = newTop + "px";

        var newDest = null;

        newTop  += tileInitCoords.height / 2;
        newLeft += tileInitCoords.width / 2;

        if (
            (newTop  > boardBCR.top  && newTop  < (boardBCR.top + boardBCR.height)) &&
            (newLeft > boardBCR.left && newLeft < (boardBCR.left + boardBCR.width))
        ) {
            var rowIndex = Math.floor(
                (
                    (newTop  - boardBCR.top)  / boardBCR.height
                ) * board.rows.length
            );

            if (rowIndex > 0 && rowIndex < board.rows.length - 1) {
                var row = board.rows[rowIndex];

                var colIndex = Math.floor(
                    (
                        (newLeft - boardBCR.left) / boardBCR.width
                    ) * row.cells.length
                );

                if (colIndex > 0 && colIndex < row.cells.length - 1) {
                    newDest = row.cells[colIndex].firstChild;
                }
            }

            if (newDest && newDest.getElementsByClassName("tile")[0]) {
                newDest = null;
            }

            if (newDest) {
                moveCMD.to = "board";
                moveCMD.indexTo = boardCells.indexOf(newDest.parentNode);
            }
        } else if (
            (newTop  > rackBCR.top  && newTop  < (rackBCR.top + rackBCR.height)) &&
            (newLeft > rackBCR.left && newLeft < (rackBCR.left + rackBCR.width))
        ) {
            var index = Math.floor(
                (
                    (newLeft - rackBCR.left) / rackBCR.width
                ) * playerLetters.length
            );

            newDest = playerLetters[index];

            if (newDest && newDest.getElementsByClassName("tile")[0]) {
                var i = index + 1, tile;

                while (playerLetters[i]) {
                    tile = playerLetters[i].getElementsByClassName("tile")[0];
                    if (!tile) {
                        var j = i;
                        while (j > index) {
                            playerLetters[j].appendChild(
                                playerLetters[j - 1].getElementsByClassName("tile")[0]
                            );
                            j--;
                        }
                        break;
                    }
                    i++;
                }

                if (newDest.getElementsByClassName("tile")[0]) {
                    i = index - 1;

                    while (playerLetters[i]) {
                        tile = playerLetters[i].getElementsByClassName("tile")[0];
                        if (!tile) {
                            var j = i;
                            while (j < index) {
                                playerLetters[j].appendChild(
                                    playerLetters[j + 1].getElementsByClassName("tile")[0]
                                );
                                j++;
                            }
                            break;
                        }
                        i--;
                    }
                }
            }

            if (newDest.getElementsByClassName("tile")[0]) {
                newDest = null;
            } else {
                moveCMD.to = "rack";
                moveCMD.indexTo = index;
            }
        } else if (
            (newTop  > bagBCR.top  && newTop  < (bagBCR.top + bagBCR.height)) &&
            (newLeft > bagBCR.left && newLeft < (bagBCR.left + bagBCR.width))
        ) {
            newDest = bag;
        }

        if (newDest !== tileDest) {
            if (tileDest) {
                tileDest.classList.remove("tile-target");
            }

            if (newDest) {
                newDest.classList.add("tile-target");
            }
        }

        tileDest = newDest;
    }

    function dragTileBegin(e) {
        loadAudio();
        if (blockMove) {
            return;
        }

        movingTile = e.currentTarget;

        tileInitMouseCoords = e;
        tileInitCoords      = movingTile.getBoundingClientRect();
        tileInitDest        = movingTile.parentNode;

        boardBCR = board.getBoundingClientRect();
        rackBCR  = rack.getBoundingClientRect();
        bagBCR   = bag.getBoundingClientRect();

        var from, index;

        var p = movingTile.parentNode;
        var oldP = movingTile;
        var oldOldP = null;

        while (p) {
            if (p === board) {
                from  = "board";
                index = boardCells.indexOf(oldOldP);

                break;
            }

            if (p === rack) {
                from  = "rack";
                index = playerLetters.indexOf(oldP);

                break;
            }

            oldOldP = oldP;
            oldP = p;
            p = p.parentNode;
        }

        moveCMD = {
            cmd: "moveLetter",
            from: from,
            indexFrom: index
        };

        mouseMove(document, dragTileMove);
        mouseUp(document,   dragTileEnd);

        movingTile.style.left   = tileInitCoords.left   + "px";
        movingTile.style.top    = tileInitCoords.top    + "px";
        movingTile.style.width  = tileInitCoords.width  + "px";
        movingTile.style.height = tileInitCoords.height + "px";

        document.body.appendChild(movingTile);
    }

    function setLetter(tile, letter, highlight) {
        tile.firstChild.textContent = letter;
        tile.lastChild.textContent = scoreOf[letter] || "";
        if (highlight) {
            tile.classList.add("tile-highlight");
            if (tilesSound.checked) {
                audioNotification.play();
            }
        } else {
            tile.classList.remove("tile-highlight");
        }
    }

    function makeLetter(letter, highlight) {
        var tile = document.createElement("span");
        tile.className = "tile";
        tile.appendChild(document.createElement("span"));
        tile.lastChild.className = "tile-letter";
        tile.appendChild(document.createElement("span"));
        tile.lastChild.className = "tile-score";
        mouseDown(tile, dragTileBegin);
        setLetter(tile, letter, highlight);
        return tile;
    }

    function setTileParent(p, letter, highlight) {
        var tile = p.getElementsByClassName("tile")[0];
        if (tile) {
            if (letter) {
                setLetter(tile, letter, highlight);
            } else {
                p.removeChild(tile);
            }
        } else if (letter) {
            tile = makeLetter(letter, highlight);
            p.appendChild(tile);
        }
    }

    function setCell(index, letter, highlight) {
        setTileParent(boardCells[index].getElementsByClassName("tile-placeholder")[0], letter, highlight);
    }

    function setBoard(board) {
        for (var i = 0; i < 15 * 15; i++) {
            setCell(i, board[i]);
        }
    }

    function set(key, value) {
        switch (key) {
            case "playerName":
                document.getElementById("name").textContent = localStorage.trivabblePlayerName = value;
                break;
            case "gameNumber":
                document.getElementById("number").textContent = localStorage.trivabbleGameNumber = value;
                break;
        }
    }

    function checkGameInProgress(f) {
        if (!gameInProgress) {
            return f();
        }

        myConfirm(
            _("Your game is not over. Are you sure you want to leave now?"),
            function () {
                myAlert(
                    format(
                        _("You are about to leave the current game. To recover it, please note its number: {0}"),
                        localStorage.trivabbleGameNumber
                    ),
                    f
                );
            }
        );
    }

    var pollingServer = false;

    function fatalError(e) {
        console.error(e);
        myConfirm(
            _("Sorry, a problem just happened. The page must be reloaded. If the problem is not too serious, you should be able to keep playing normally. Otherwise, contact the person who is able to fix the problem. Click on “Yes” to reload the page."),
            function () {
                location.reload();
            }, function () {
                blockMove = 1000;
                document.getElementById("panel").appendChild(document.createElement("a"));
                document.getElementById("panel").lastChild.href = "#";
                document.getElementById("panel").lastChild.onclick = location.reload.bind(location);
                document.getElementById("panel").lastChild.textContent = _("Pour continuer à jouer, cliquez ici");
            }
        );
    }

    function setRackCell(index, letter) {
        setTileParent(playerLetters[index], letter);
    }

    function blinkTransitionEnd(element) {
        element.removeEventListener("animationend", element._te, false);
        element.classList.remove("blink");
    }

    function blink(element) {
        element._te = blinkTransitionEnd.bind(null, element);
        element.addEventListener("animationend", element._te, false);
        setTimeout(element._te, 5000);
        element.classList.add("blink");
    }

    function handleReceivedData(data) {
        if (data.msg) {
            chatMessages.appendChild(
                document.createElement("div")
            );

            chatMessages.lastChild.className = "msg";
            chatMessages.appendChild(document.createElement("span"));
            chatMessages.lastChild.className = "msg-sender";
            chatMessages.lastChild.textContent = _("{0} : ").replace("{0}", data.msg.sender);

            chatMessages.appendChild(document.createElement("span"));
            chatMessages.lastChild.className = "msg-content";
            chatMessages.lastChild.textContent = data.msg.content;

            chatMessages.scrollTop = chatMessages.scrollHeight;

            if (data.msg.sender !== localStorage.trivabblePlayerName) {
                if (msgSound.checked) {
                    audioChat.play();
                }

                blink(chatMessages);
            }
        }

        if (data.players) {
            if (participantPlaceholder) {
                participantPlaceholder.parentNode.removeChild(participantPlaceholder);
                participantPlaceholder = null;
            }

            for (var i = 0; i < data.players.length; i++) {
                var player = data.players[i];
                var playerName = data.players[i].player;

                if (!tablePlayers[playerName]) {
                    var before = null;

                    for (var j = 1;  j < participants.rows.length; j++) {
                        if (playerName < participants.rows[j].cells[0].textContent) {
                            before = participants.rows[j];
                            break;
                        }
                    }

                    var row = document.createElement("tr");
                    participants.insertBefore(row, before);
                    row.appendChild(document.createElement("td"));
                    row.lastChild.textContent = playerName;
                    row.appendChild(document.createElement("td"));
                    row.appendChild(document.createElement("td"));

                    row.lastChild.onclick = (
                        function (row) {
                            return function () {
                                myPrompt(
                                    format(
                                        _("Enter the new score of {0}:"),
                                        row.firstChild.textContent
                                    ),
                                    function (res) {
                                        res = parseInt(res);
                                        if (!isNaN(res)) {
                                            sendCmds([{
                                                cmd:"score",
                                                player:row.firstChild.textContent,
                                                score: res
                                            }]);
                                        }
                                    },
                                    row.lastChild.textContent
                                );
                            }
                        }
                    )(row);

                    tablePlayers[playerName] = row;
                }

                if (player.hasOwnProperty("score")) {
                    var scoreCell = tablePlayers[playerName].childNodes[2];
                    scoreCell.textContent = player.score;
                    blink(scoreCell);
                }

                if (player.hasOwnProperty("rackCount")) {
                    var countCell = tablePlayers[playerName].childNodes[1];
                    countCell.textContent = player.rackCount;
                    blink(countCell);
                }
            }
        }

        if (data.date) {
            if (!data.sync && lastDate && lastDate > data.date) {
                setTimeout(sendCmds, 200, [{cmd:"sync"}]);
                return;
            } else {
                lastDate = data.date;
            }
        }

        if (data.playerName) {
            set("playerName", data.playerName);
        }

        if (data.gameNumber) {
            set("gameNumber", data.gameNumber);
        }


        if (data.letterValues) {
            scoreOf = data.letterValues;
        }

        if (data.board) {
            setBoard(data.board);
        }

        if (typeof data.remainingLetters === "number") {
            document.getElementById("remaining-letters").textContent = data.remainingLetters;
            if (data.remainingLetters === 0) {
                helpBag.style.display = "none";
                helpClear.style.display = "";
            } else {
                helpBag.style.display = "";
                helpClear.style.display = "none";
            }
        }

        if (data.rack) {
            setRack(data.rack);
        }


        switch(data.action) {
            case "pushBag": //TODO
                break;

            case "popBag": //TODO
                break;

            case "reset": //TODO
                tablePlayers = {};
                while (participants.rows[1]) {
                    participants.removeChild(participants.rows[1]);
                }
                sendCmds([{"cmd":"hello"}])
                break;

            case "moveLetter":
                if (data.from === "board") {
                    setCell(data.indexFrom, "");
                } else if (data.from === "rack") {
                    setRackCell(data.indexFrom, "");
                }

                if (data.to === "board") {
                    setCell(data.indexTo, data.letter, data.hasOwnProperty("player") && data.player !== localStorage.trivabblePlayerName);
                } else if (data.to === "rack") {
                    setRackCell(data.indexTo, data.letter);
                }
                break;
            case "setCell":
                setCell(data.indexTo, data.letter, data.hasOwnProperty("player") && data.player !== localStorage.trivabblePlayerName);
                break;
            case "setRackCell":
                setRackCell(data.indexTo, data.letter);
        }
    }

    function send(data) {
        var xhr = new XMLHttpRequest();

        xhr.open("POST", "/:trivabble", true);
        xhr.setRequestHeader("Content-Type", "text/plain");
        xhr.send(JSON.stringify(data));

        var thisRequestIsPolling = false, currentIndex = 0, expectedLength = 0;

        if (!data.cmds || !data.cmds.length) {
            pollingServer = true;
            thisRequestIsPolling = true;
        }

        if (!thisRequestIsPolling) {
            blockMove++;
        }

        xhr.onreadystatechange = function () {
            // TODO error handling

            if (xhr.readyState === 3 && thisRequestIsPolling) {
                while (true) {
                    if (!expectedLength) {
                        var i = currentIndex;
                        while (i < xhr.responseText.length) {
                            if ("0123456789".indexOf(xhr.responseText.charAt(i)) === -1) {
                                expectedLength = parseInt(xhr.responseText.substring(currentIndex, i));
                                currentIndex = i;
                                break;
                            }
                            ++i;
                        }
                    }

                    if (expectedLength && (xhr.responseText.length >= currentIndex + expectedLength)) {
                        var end = currentIndex + expectedLength;
                        try {
                            var msgs = JSON.parse(
                                xhr.responseText.substring(
                                    currentIndex,
                                    end
                                )
                            );
                            currentIndex = end;
                            expectedLength = 0;
                        } catch (e) {
                            console.error(
                                xhr.responseText.substring(
                                    currentIndex,
                                    end
                                )
                            );

                            fatalError(e);
                        }

                        if (msgs instanceof Array) {
                            for (var i = 0; i < msgs.length; i++) {
                                handleReceivedData(msgs[i]);
                            }
                        } else {
                            handleReceivedData(msgs);
                        }
                    } else {
                        break;
                    }
                }
            } else if (xhr.readyState === 4) {
                if (thisRequestIsPolling) {
                    pollingServer = false;
                    setTimeout(sendCmds.bind(null, []), POLLING_DELAY);
                    return;
                }

                try {
                    var res = JSON.parse(xhr.responseText);
                    if (res instanceof Array) {
                        for (var i = 0; i < res.length; i++) {
                            handleReceivedData(res[i]);
                        }
                    } else {
                        handleReceivedData(res);
                    }
                } catch (e) {
                    fatalError(e);
                }

                blockMove--;

                if (!pollingServer) {
                    sendCmds([]);
                }
            }
        };
    }

    function sendCmds(cmds) {
        send({
            gameNumber: localStorage.trivabbleGameNumber || "",
            playerName: localStorage.trivabblePlayerName,
            cmds: cmds
        });
    }

    function joinGame() {
        checkGameInProgress(
            function () {
                myPrompt(
                    _("To join a game, please give the number which is displayed on your adversary(ies)' screen.\nIf you do not know it, ask them.\n\nWarning: your adversary must not take your number, (s)he must keep his/her own. If you whish to recover your current game, please not the following number: {0}.").replace("{0}", localStorage.trivabbleGameNumber),
                    function (n) {
                        n = parseInt(n);
                        if (isNaN(n)) {
                            myAlert(_("It seems your did not give a correct number, or you clicked on “Cancel”. As a result, the current game continues, if any. To join a game, click on “Join a game” again."));
                        } else {
                            localStorage.trivabbleGameNumber = n;
                            location.reload();
                        }
                    }
                );
            }
        );
    }

    var specialTypesText = {
        "doubleLetter" : _("Double\nLetter"),
        "doubleWord"   : _("Double\nWord"),
        "tripleLetter" : _("Triple\nLetter"),
        "tripleWord"   : _("Triple\nWord")
    };

    function specialCell(type, cell) {
        cell.firstChild.appendChild(document.createElement("span"));
        cell.classList.add("special-cell");
        cell.classList.add("special-cell-" + type);
        cell.lastChild.lastChild.textContent = _(specialTypesText[type]);
        cell.lastChild.lastChild.className = "special-cell-label";
    }

    function changeName() {
        myPrompt(
            _("To change your name, enter a new one. You can keep using your current name by cancelling. Please note that if you change your name and you have games in progress, you will not be able to keep playing them anymore unless you get back to your current name."),
            function (newName) {
                if (newName && newName.trim()) {
                    localStorage.trivabblePlayerName = newName.trim();
                    document.getElementById("name").textContent = localStorage.trivabblePlayerName;
                }
            },
            localStorage.trivabblePlayerName
        );
    }

    function startGame(number) {
        if (number) {
            localStorage.trivabbleGameNumber = number;
        }
        sendCmds([{cmd:"joinGame", lang:libD.lang}]);
    }

    var audioTileLoaded = false, audioMsgLoaded = false, tilesSound, msgSound, langSelector;

    function loadAudio() {
        if (!audioTileLoaded && tilesSound.checked) {
            audioTileLoaded = true;
            audioNotification.load();
        }

        if (!audioMsgLoaded  && msgSound.checked) {
            audioMsgLoaded = true;
            audioChat.load();
        }
    }

    function bagClicked() {
        loadAudio();

        if (blockMove) {
            return;
        }

        var index = getFreeRackSpaceIndex();
        if (index === -1) {
            myAlert(_("You cannot take another tile: your rack is full."));
        } else {
            sendCmds(
                [{cmd:"moveLetter", from: "bag", to: "rack", indexTo: index}]
            );
        }
    }

    function clearGame() {
        myConfirm(
            _("Are you sure you want to put all the tiles back in the bag (un order to play another game)?"),
            function () {
                sendCmds(
                    [{cmd: "resetGame"}]
                );
            }
        )
    }

    function clearRack() {
        myConfirm(
            _("Are you sure you want to put all your tiles back in the bag?"),
            function () {
                sendCmds(
                    [
                        {cmd: "moveLetter", from: "rack", indexFrom: 0, to: "bag", indexTo: -1},
                        {cmd: "moveLetter", from: "rack", indexFrom: 1, to: "bag", indexTo: -1},
                        {cmd: "moveLetter", from: "rack", indexFrom: 2, to: "bag", indexTo: -1},
                        {cmd: "moveLetter", from: "rack", indexFrom: 3, to: "bag", indexTo: -1},
                        {cmd: "moveLetter", from: "rack", indexFrom: 4, to: "bag", indexTo: -1},
                        {cmd: "moveLetter", from: "rack", indexFrom: 5, to: "bag", indexTo: -1},
                        {cmd: "moveLetter", from: "rack", indexFrom: 6, to: "bag", indexTo: -1}
                    ]
                );
            }
        );
    }

    function initChat() {
        chatMessages.style.width = chatMessages.offsetWidth + "px";

        var btn = document.getElementById("chat-btn");
        var ta = document.getElementById("chat-ta");

        ta.onmouseup = function () {
            chatMessages.style.width = ta.offsetWidth + "px";
        };

        btn.onclick = function () {
            loadAudio();
            sendCmds([
                {
                    cmd: "msg",
                    msg: ta.value
                }
            ]);

            ta.value = "";
        };

        ta.onkeydown = function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                e.stopPropagation();
                btn.onclick();
            }
        };
    }

    function initSound() {
        audioNotification = new Audio();
        audioNotification.preload = "auto";
        audioNotification.volume  = 1;

        var audioSourceOGG = document.createElement("source");
        audioSourceOGG.src = "notification.ogg";

        var audioSourceMP3 = document.createElement("source");
        audioSourceMP3.src = "notification.mp3";
        audioNotification.appendChild(audioSourceOGG);
        audioNotification.appendChild(audioSourceMP3);

        audioChat = new Audio();
        audioChat.preload = "auto";
        audioChat.volume  = 1;

        audioSourceOGG = document.createElement("source");
        audioSourceOGG.src = "receive.ogg";

        var audioSourceMP3 = document.createElement("source");
        audioSourceMP3.src = "receive.mp3";
        audioChat.appendChild(audioSourceOGG);
        audioChat.appendChild(audioSourceMP3);


        tilesSound = document.getElementById("tiles-sound");
        msgSound = document.getElementById("msg-sound");
        langSelector = document.getElementById("select-lang")

        tilesSound.onclick = function () {
            localStorage.trivabbleTileSound = tilesSound.checked;
        };

        msgSound.onclick = function () {
            localStorage.trivabbleMsgSound = msgSound.checked;
        };

        langSelector.onchange = function() {
          document.cookie = 'lang=' + langSelector.value;
          document.location.reload();
        }

        if (localStorage.hasOwnProperty("trivabbleMsgSound")) {
            msgSound.checked = localStorage.trivabbleMsgSound === "true";
        } else {
            localStorage.trivabbleMsgSound = msgSound.checked;
        }

        if (localStorage.hasOwnProperty("trivabbleTileSound")) {
            tilesSound.checked = localStorage.trivabbleTileSound === "true";
        } else {
            localStorage.trivabbleTilesSound = tilesSound.checked;
        }
    }

    function repromptName(f) {
        if (localStorage.trivabblePlayerName && localStorage.trivabblePlayerName.trim()) {
            f();
        } else {
            myPrompt(
                _("It seems your did not give your name. You need to do it for the game to run properly."),
                function (name) {
                    if (name && name.trim()) {
                        localStorage.trivabblePlayerName = name.trim();
                    }

                    repromptName(f);
                }
            );
        }
    }

    trivabble.applyL10n = function () {
        var n = document.querySelectorAll("[data-l10n]");

        for (var i = 0, len = n.length; i < len; i++) {
            if (n[i].dataset.l10n === "text-content") {
                n[i].textContent = _(n[i].textContent.trim());
            } else {
                n[i].setAttribute(n[i].dataset.l10n, _(n[i].getAttribute(n[i].dataset.l10n)));
            }
        }

        trivabble.run();
    };

    function initGlobals() {
        board         = document.getElementById("board");
        rack          = document.getElementById("rack");
        participants  = document.getElementById("participants");
        name          = document.getElementById("name");
        bag           = document.getElementById("bag");
        chatMessages  = document.getElementById("chat-messages");
        helpBag       = document.getElementById("help-bag");
        helpClear     = document.getElementById("help-clear");
        participantPlaceholder = document.getElementById("participants-placeholder");
    }

    function initEvents() {
        mouseDown(bag, bagClicked);

        document.getElementById("clear-game").onclick  = clearGame;
        document.getElementById("change-name").onclick = changeName;
        document.getElementById("join-game").onclick   = joinGame;
        document.getElementById("clear-rack").onclick  = clearRack;
        helpClear.onclick  = clearGame;
    }

    function initGame() {
        if (!localStorage.trivabblePlayerName) {
            myPrompt(
                _("Hello! To begin, enter your name. Your adversaries will see this name when you play with them."),
                function (name) {
                    if (name && name.trim()) {
                        localStorage.trivabblePlayerName = name;
                    }
                    repromptName(initGame);
                }
            );

            return;
        }

        document.getElementById("name").textContent = localStorage.trivabblePlayerName;

        var letters = "ABCDEFGHIJKLMNO";

        var doubleLetter = {
            "0,3" : true,
            "0,11": true,
            "2,6" : true,
            "2,8" : true,
            "3,0" : true,
            "3,7" : true,
            "3,14": true,
            "6,2" : true,
            "6,6" : true,
            "6,8" : true,
            "6,12": true
        };

        var cell;

        for (var i = 0; i < 7; i++) {
            var span = document.createElement("span");
            span.className = "tile-placeholder";
            rack.appendChild(span);
            playerLetters.push(span);
        }

        for (var i = 0; i < 15; i++) {
            board.rows[0].appendChild(document.createElement("th"));
            board.rows[0].lastChild.textContent = i + 1;

            board.appendChild(document.createElement("tr"));
            board.lastChild.appendChild(document.createElement("th"));
            board.lastChild.lastChild.textContent = letters[i];
            for (var j = 0; j < 15; j++) {
                cell = document.createElement("td");
                boardCells.push(cell);
                board.lastChild.appendChild(cell);
                cell.appendChild(document.createElement("div"));
                cell.lastChild.className = "tile-placeholder";

                if (i === j && i === 7) {
                    specialCell("doubleWord", board.lastChild.lastChild);
                    var cell = board.lastChild.lastChild.getElementsByClassName("special-cell-label")[0];
                    cell.textContent = "★";
                    board.lastChild.lastChild.id = "center-cell";
                } else if (i % 7 === 0 && j % 7 === 0) {
                    specialCell("tripleWord", board.lastChild.lastChild);
                } else if ((i === j || i + j === 14) && (i < 5 || i > 9)) {
                    specialCell("doubleWord", board.lastChild.lastChild);
                } else if ((i % 4 === 1) && (j % 4 === 1)) {
                    specialCell("tripleLetter", board.lastChild.lastChild);
                } else if ((i < 8 && doubleLetter[i + "," + j]) || (i > 7 && doubleLetter[(14-i) + "," + j]) || (i === 7 && (j === 3 || j === 11))) {
                    specialCell("doubleLetter", board.lastChild.lastChild);
                }
            }

            board.lastChild.appendChild(document.createElement("th"));
            board.lastChild.lastChild.textContent = letters[i];
        }

        board.rows[0].appendChild(board.rows[0].cells[0].cloneNode(false));

        board.appendChild(document.createElement("tr"));
        board.lastChild.appendChild(board.rows[0].cells[0].cloneNode(false));

        for (var i = 0; i < 15; i++) {
            board.lastChild.appendChild(document.createElement("th"));
            board.lastChild.lastChild.textContent = i + 1;
        }

        board.lastChild.appendChild(board.rows[0].cells[0].cloneNode(false));

        if (localStorage.trivabbleGameNumber) {
            document.getElementById("number").textContent = localStorage.trivabbleGameNumber;
        }

        startGame(localStorage.trivabbleGameNumber);
    }

    trivabble.run = function () {
        initGlobals();
        initEvents();
        initChat();
        initGame();
        initSound();
    };

    trivabble.l10nError = trivabble.run;
}());
